﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Force : BaseButton {

	// Use this for initialization
	void Start () {
	    material = GetComponent<Renderer>();
    }
	
	// Update is called once per frame
	void Update () {
	    if (Selected)
	    {

	        material.material.color = Color.blue;
	    }
	    else
	    {
	        material.material.color = Color.white;
	    }

	    if (Triggererd)
	        GameController.isSlider = !GameController.isSlider;
	}
}
