﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class player : MonoBehaviour
{

    Rigidbody rb;
    public CameraController head;
    public GameObject ScoreText;
    // Use this for initialization
    private const int RIGHT_ANGLE = 90;

    // This variable determinates if the player will move or not 
    private bool isWalking = false;
    public GameObject coinPickUp;
    public GameObject HitSound;
    public GameObject GameOverSound;
    public GameObject HeadShotSound;
    public GameObject FirstKillSond;
    public GameObject DoomUI;
    //This is the variable for the player speed
    [Tooltip("With this speed the player will move.")]
    public float speed = 0.5f;

    [Tooltip("Activate this checkbox if the player shall move when the Cardboard trigger is pulled.")]
    public bool walkWhenTriggered;

    [Tooltip("Activate this checkbox if the player shall move when he looks below the threshold.")]
    public bool walkWhenLookDown;

    [Tooltip("This has to be an angle from 0° to 90°")]
    public double thresholdAngle = 50;

    [Tooltip("Activate this Checkbox if you want to freeze the y-coordiante for the player. " +
             "For example in the case of you have no collider attached to your CardboardMain-GameObject" +
             "and you want to stay in a fixed level.")]
    public bool freezeYPosition;

    [Tooltip("This is the fixed y-coordinate.")]
    public float yOffset;

    private Vector3 initPosition;
    private LinkedList<ActionNode> rootNode;
    private List<GameObject> disabledCoins = new List<GameObject>();
    private List<GameObject> allEnemies = new List<GameObject>();
    private bool revind = false;
    private int TotalCoinsCount = 0;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        initPosition = transform.position;
        rootNode = new LinkedList<ActionNode>();
        TotalCoinsCount = GameObject.FindGameObjectsWithTag("Coin").Length;
        foreach(var enemy in GameObject.FindGameObjectsWithTag("DoomBad"))
        {
            enemy.SetActive(false);
            allEnemies.Add(enemy);
        }
        ResetWorld();
    }

    public int Score;

    void StopWorld()
    {
        var coins = GameObject.FindGameObjectsWithTag("Coin");
        foreach (var coin in coins)
        {
            coin.GetComponent<CoinController>().Rotate = false;
            coin.GetComponent<CoinController>().Reverse = revind;
        }

        var traps = GameObject.FindGameObjectsWithTag("Trap");
        foreach (var trap in traps)
        {
            var animation = trap.GetComponent<Animation>();
            if (animation == null)
                continue;
            animation.Stop();
        }
        
    }

    void ResumWorld()
    {
        var coins = GameObject.FindGameObjectsWithTag("Coin");
        foreach (var coin in coins)
        {
            coin.GetComponent<CoinController>().Rotate = true;
            coin.GetComponent<CoinController>().Reverse = revind;
        }


        var traps = GameObject.FindGameObjectsWithTag("Trap");
        foreach (var trap in traps)
        {
            var animation = trap.GetComponent<Animation>();
            if (animation == null)
                continue;
            animation.Play();
        }
    }

    void ResetWorld()
    {
        isWalking = false;
        foreach (var coin in disabledCoins)
        {
            coin.SetActive(true);
        }
        var audio = this.GetComponent<AudioSource>();
        if (audio != null)
        {
            audio.Stop();
        }
        DoomUI.SetActive(false);
        foreach (var enemy in allEnemies)
        {
            enemy.SetActive(false);
        }
        StopWorld();
        Score = 0;
        UpdateScore();
        transform.position = initPosition;
        rootNode.Clear();
        //var coins = GameObject.FindGameObjectsWithTag("Coin");

    }

    void StartRevind()
    {
        revind = true;

        var traps = GameObject.FindGameObjectsWithTag("Trap");
        foreach (var trap in traps)
        {
            var animation = trap.GetComponent<Animation>();
            if (animation == null)
                continue;
            animation.Rewind();
        }

        

    }

    void Update()
    {

        try
        {
            // transform.rotation. = head.transform.rotation.y;
            //transform.LookAt(transform.position + head.transform.rotation * Vector3.forward); ;
            // Walk when the Cardboard Trigger is used 
            if (walkWhenTriggered && !walkWhenLookDown && !isWalking)
            {
                isWalking = true;
                ResumWorld();
            }
            else if (walkWhenTriggered && !walkWhenLookDown && isWalking)
            {
                isWalking = false;
                StopWorld();
            }

            if (!revind && head.transform.eulerAngles.z >= thresholdAngle && head.transform.eulerAngles.z <= thresholdAngle+10)
            {
                StartRevind();
            }
            if (head.transform.eulerAngles.z > 350 - thresholdAngle && head.transform.eulerAngles.z < 360 - thresholdAngle)
            {
                var audio = this.GetComponent<AudioSource>();
                if (audio != null)
                {
                    audio.Play();
                }
                DoomUI.SetActive(true);
                foreach (var enemy in allEnemies)
                    enemy.SetActive(true);
            }
            if (head.transform.eulerAngles.y >= 10)
            {

            }

            // Walk when player looks below the threshold angle 
            if (walkWhenLookDown && !walkWhenTriggered && !isWalking &&
                head.transform.eulerAngles.x >= (thresholdAngle) &&
                head.transform.eulerAngles.x <= RIGHT_ANGLE)
            {
                isWalking = true;
                ResumWorld();
            }
            else if (walkWhenLookDown && !walkWhenTriggered && isWalking &&
                     (head.transform.eulerAngles.x <= thresholdAngle ||
                      head.transform.eulerAngles.x >= RIGHT_ANGLE))
            {
                isWalking = false;
                StopWorld();
            }

            //Walk when the Cardboard trigger is used and the player looks down below the threshold angle
            if (walkWhenLookDown && walkWhenTriggered && !isWalking &&
                head.transform.eulerAngles.x >= thresholdAngle &&

                head.transform.eulerAngles.x <= RIGHT_ANGLE)
            {
                isWalking = true;
                ResumWorld();
            }
            else if (walkWhenLookDown && walkWhenTriggered && isWalking &&
                     head.transform.eulerAngles.x <= (360 - thresholdAngle) &&
                     (
                         head.transform.eulerAngles.x >= RIGHT_ANGLE))
            {
                isWalking = false;
                StopWorld();
            }

            if (isWalking)
            {
                var coins = GameObject.FindGameObjectsWithTag("Coin");
                foreach (var coin in coins)
                {
                    coin.GetComponent<CoinController>().Reverse = revind;
                }
                foreach (var coin in disabledCoins)
                {
                    coin.GetComponent<CoinController>().Reverse = revind;
                }
                if (revind)
                {
                    RevindStep();
                    return;
                }

                Vector3 direction = new Vector3(head.transform.forward.x, 0, head.transform.forward.z).normalized *
                                    speed * Time.deltaTime;
                Quaternion rotation = Quaternion.Euler(new Vector3(0, -transform.rotation.eulerAngles.y, 0));
                if (PlayerCollided)
                    return;
                transform.Translate(rotation * direction);
                //rb.AddForce(rotation * direction);
                rootNode.AddLast(new ActionNode()
                {
                    Position = transform.position,
                });
            }

            if (freezeYPosition)
            {
                transform.position = new Vector3(transform.position.x, yOffset, transform.position.z);
            }

        }
        finally
        {
            UpdateScore();
        }
    }

    private void RevindStep()
    {
        if (rootNode.Count == 0)
        {
            revind = false;
            isWalking = false;
            StopWorld();
            return;
        }

        var node = rootNode.Last.Value;
        var position = node.Position;
        rootNode.RemoveLast();
        transform.position = position;
        if (node.Action == ActionType.GetCoin)
        {
            Score--;
            node.Item.SetActive(true);
        }
        return;
    }

    private void UpdateScore()
    {
        ScoreText.GetComponent<Text>().text = string.Format("Score: {0}/{1}", Score, TotalCoinsCount+10);
    }

    public bool PlayerCollided;

    void OnTriggerEnter(Collider other)
    {
        try
        {
            if (other.gameObject.CompareTag("Coin"))
            {
                Score++;
                
                other.gameObject.SetActive(false);
                var audio = coinPickUp.GetComponent<AudioSource>();
                if (audio != null)
                {
                    audio.Play();
                    
                }
                disabledCoins.Add(other.gameObject);
                rootNode.AddLast(new ActionNode()
                {
                    Position = transform.position,
                    Action = ActionType.GetCoin,
                    Item = other.gameObject,
                });
                UpdateScore();
            }

            else if (other.gameObject.CompareTag("Wall"))
            {
                var audio = HitSound.GetComponent<AudioSource>();
                if (audio != null)
                    audio.Play();

                transform.Rotate(new Vector3(0, 180, 0));

            }
            else if (other.gameObject.CompareTag("Trap"))
            {
                var audio = GameOverSound.GetComponent<AudioSource>();
                if (audio != null)
                    audio.Play();
                ResetWorld();
            }
            else if (other.gameObject.CompareTag("DoomBad"))
            {
                other.gameObject.SetActive(false);
                if (allEnemies.Count(x => x.activeSelf) == allEnemies.Count - 1)
                {
                    var audio = FirstKillSond.GetComponent<AudioSource>();
                    if (audio != null)
                        audio.Play();
                }
                else
                {
                    var audio = HeadShotSound.GetComponent<AudioSource>();
                    if (audio != null)
                        audio.Play();

                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    private class ActionNode
    {
        public ActionType Action { get; set; }
        public GameObject Item { get; set; }
        public Vector3 Position { get; set; }
    }

    public enum ActionType
    {
        Move,
        GetCoin,
    }




}
