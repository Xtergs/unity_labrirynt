﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIOverlay : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
	    head = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>();
	}

    public CameraController head;

    // Update is called once per frame
    void Update () {
        Vector3 direction = new Vector3(head.transform.forward.x, 0, head.transform.forward.z);
        Quaternion rotation = Quaternion.Euler(new Vector3(0, -transform.rotation.eulerAngles.y, 0));
        transform.Translate(rotation * direction);
    }
}
