﻿using UnityEngine;

public class PlayButton : BaseButton
{

    // Use this for initialization

    void Start()
    {
        material = GetComponent<Renderer>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Selected)
        {

            material.material.color = Color.blue;
        }
        else
        {
            material.material.color = Color.white;
        }
    }
}
