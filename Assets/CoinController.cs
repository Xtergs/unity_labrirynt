﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Rotate = false;
	}
	
	// Update is called once per frame

    public bool Rotate;
    public bool Reverse = false;
	void Update ()
	{
	    int dir = 1;
	    if (!Reverse)
	        dir = -1;
        if (Rotate)
	        transform.Rotate(new Vector3(0, 0, Time.deltaTime * 100 * dir));
	}
}
