﻿using UnityEngine;

public class Slider : MonoBehaviour
{

    // Use this for initialization
    private float StartPosition = -0.47f;
    private float EndPosition = 0.45f;
    public float Position;
    public float delata = 0.01f;
    private int direction = 1;

    public float GetValue()
    {
        return (Position - StartPosition) / (EndPosition - StartPosition);
    }
    void Start()
    {
        StartPosition = transform.position.x;
        EndPosition = StartPosition + 1;

        InvokeRepeating("UpdatePosition", 0, 1.0f);
    }

    // Update is called once per frame
    void Update()
    {
        
        
    }

    void UpdatePosition()
    {
        if (GameController.isSlider == false)
            return;
        var tempPost = transform.position;
        if (tempPost.x >= EndPosition)
            direction = -1;
        if (tempPost.x <= StartPosition)
            direction = 1;
        tempPost.x += delata * direction;
        Debug.Log(tempPost.x);
        transform.Translate(delata * direction, 0, 0);
        Debug.Log(transform.position);
    }
}
