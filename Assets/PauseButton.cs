﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseButton : BaseButton {

    // Use this for initialization
    void Start()
    {
        material = GetComponent<Renderer>();

    }

    private Renderer material;

    // Update is called once per frame
    void Update()
    {
        if (Selected)
        {

            material.material.color = Color.blue;
        }
        else
        {
            material.material.color = Color.white;
        }
    }
}
