﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class GameController : MonoBehaviour {

    // Use this for initialization
    public Car car;

    private bool moving = false;

    public static bool isPlay { get; set; }
    public static bool isPause { get; set; }
    public static bool isForward { get; set; }
    public static bool isSlider { get; set; }

    public GameObject Aim;
    private DateTime lastSelect { get; set; }

    void Start () {
		
	}

    private BaseButton lastSelected;
	
	// Update is called once per frame
	void Update ()
	{
	    RaycastHit hit;
	    if (Physics.Raycast(Aim.transform.position, Aim.transform.forward, out hit))
	    {
	        var button = hit.transform.GetComponent<BaseButton>();

            if (button != null)
            {
                if (lastSelected != null && lastSelected != button)
                {
                    lastSelected.Selected = false;
                    lastSelected.Triggererd = false;
                }

                if (button.Selected && (DateTime.UtcNow - lastSelect).TotalSeconds > 3)
                {
                    button.Triggererd = true;
                }

                if (button.Selected == false)
                {
                    button.Selected = true;
                    lastSelected = button;
                    lastSelect = DateTime.UtcNow;
                    Debug.Log(string.Format("{0}, {1}, {2}", button.Selected, button.Triggererd, lastSelect));
                }
            }
            else
            {
               
            }
	    }
	    else
	    {
	        if (lastSelected != null)
	        {
	            lastSelected.Selected = false;
	            lastSelected.Triggererd = false;
	            lastSelected = null;
	        }
	    }
	    if (Input.GetKeyDown("space"))
	    {
	        moving = !moving;
	        if (moving)
	            car.GetComponent<Rigidbody>().AddForce(0, 10, 100);
	    }

	    if (moving)
	    {
	        //car.transform.Translate(car.transform.forward);
	    }
	}
}
