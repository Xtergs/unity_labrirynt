﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cup : MonoBehaviour
{

    public float downHeight = 0;

    public float upHeight = 100;

    private Vector3 targetPosition;
	// Use this for initialization
	void Start ()
	{
	    targetPosition = new Vector3(transform.position.x, upHeight, transform.position.z);
	}
	
	// Update is called once per frame
	void Update ()
	{
	    transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime);
	}
}
